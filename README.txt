 * Introduction

    Views Argument Globals is a Views plugin that implements a clean way to retrieve information from the PHP global variables, and pass like a "contextual filter" also known a argument

    - Do you need to filter a view result from a value inside a $GLOBALS variable?
    - Do you need to filter a view result from a value inside a $_GET variable?
    - Do you need to filter a view result from a value inside a $_COOKIE variable?
    - Do you need to filter a view result from a value inside a $_SESSION variable?

    This is module for you!

 * Requirements

    - views

 * Similar projects

    Both modules bellow implements a way to retrieve information from GET or other HTTP methods, but no of them implements a way to retrieve a variable inside a $_SESSION or a $GLOBALS.

    - views_arguments_extras
    - views_contextual_filter_query


 * Installation

    Only that you need do is grab this package and put in your properly modules folder inside your drupal site instalation, an go to Administration >> Modules, find "Views Argument Globals" and enable it.

    Or use drush like that, in your drupal instalation path:

      - drush en views_argument_globals

 * Configuration

    After instalation only that you need do is in your views display, inside a Advanced >> Contextual Filters >> Choose a option between these:

      - $GLOBALS variable
      - $_GET variable
      - $_COOKIE variable
      - $_SESSION variable

    Follow the instructions in a helper text and save configuration, only that!

 * Maintainers

    João Paulo Seregatte Costa (https://www.drupal.org/u/seregatte)

 * TODO

    Submit to a full project on (https://www.drupal.org/project/issues/projectapplications)