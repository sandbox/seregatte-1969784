<?php

/**
 * @file
 * Primarily Drupal hooks and global API functions to manipulate views.
 */

/**
 * Implements hook_views_plugins().
 */
function views_argument_globals_views_plugins() {
  return array(
    'module' => 'views_argument_globals',
    'argument default' => array(
      'globals_variable' => array(
        'title' => t('$GLOBALS variable'),
        'handler' => 'ViewsPluginArgumentDefaultGlobals',
        'path' => drupal_get_path('module', 'views_argument_globals') . '/includes',
        'parent' => 'fixed',
      ),
      'session_variable' => array(
        'title' => t('$_SESSION variable'),
        'handler' => 'ViewsPluginArgumentDefaultSession',
        'path' => drupal_get_path('module', 'views_argument_globals') . '/includes',
        'parent' => 'fixed',
      ),
      'cookie_variable' => array(
        'title' => t('$_COOKIE variable'),
        'handler' => 'ViewsPluginArgumentDefaultCookie',
        'path' => drupal_get_path('module', 'views_argument_globals') . '/includes',
        'parent' => 'fixed',
      ),
      'get_variable' => array(
        'title' => t('$_GET variable'),
        'handler' => 'ViewsPluginArgumentDefaultGet',
        'path' => drupal_get_path('module', 'views_argument_globals') . '/includes',
        'parent' => 'fixed',
      ),
    ),
  );
}
