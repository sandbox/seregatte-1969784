<?php

/**
 * @file
 * Contains the session argument default plugin.
 */

/**
 * Default argument plugin to use the raw value from the URL.
 *
 * @ingroup views_argument_default_plugins
 */
class ViewsPluginArgumentDefaultGlobals extends views_plugin_argument_default {

  /**
   * Provide the default form form for validating options.
   *
   * @param array $form
   *   Array of drupal form api.
   * @param string $form_state
   *   Values in form.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    // Using range(1, 10) will create an array keyed 0-9, which allows arg() to
    // properly function since it is also zero-based.
    $form['argument'] = array(
      '#type' => 'textfield',
      '#title' => t('Global Key'),
      '#default_value' => $this->options['argument'],
      '#description' => t('Example:') . '<br/>' .
      t('$GLOBALS[variable] = variable') . '<br/>' .
      t('$GLOBALS[arr][var] = arr:var') . '<br/>' .
      t('$GLOBALS[arr][obj]->var = arr:obj:var'),
    );
    $form['multiple'] = array(
      '#type' => 'select',
      '#title' => t('Multiple values'),
      '#options' => array(
        'no' => t('No implode, use first element of array'),
        '+' => t('Implode values of 1+2+3'),
        ',' => t('Implode values of 1,2,3'),
      ),
      '#default_value' => $this->options['multiple'],
      '#description' => t('If variable is array implode array?<br/> When 
        multiple options enable, Check if "Allow multiple values" in More 
        options behind is enabled.'),
    );
  }

  /**
   * Retrieve the options when this is a new access control plugin.
   * @return array 
   *   Options of form.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['argument'] = array('default' => '');
    $options['multiple'] = array('default' => 'no');
    return $options;
  }

  /**
   * Return the default argument.
   *
   * This needs to be overridden by every default argument handler to properly 
   * do what is needed.
   */
  public function get_argument() {
    $_current = $this->getCurrent();
    $value = $this->convertParameter($_current);
    if (isset($value)) {
      if (is_array($value) && $this->options['multiple'] != 'no') {
        return implode($this->options['multiple'], $value);
      }
      elseif (is_array($value)) {
        return current($value);
      }
      else {
        return $value;
      }
    }
    else {
      return $this->argument->options['exception']['value'];
    }
  }

  /**
   * Return the current argument.
   */
  protected function getCurrent() {
    $key = strtoupper(preg_replace('%(.*)\_.*%', '_$1', $this->definition['name']));
    return ($key == '_GLOBALS')? $GLOBALS : $GLOBALS[$key];
  }

  /**
   * Return the variable session argument.
   */
  protected function convertParameter($_current) {
    $argument = $this->options['argument'];
    if (preg_match("%:%", $argument)) {
      $array_map = preg_split("%:%", $argument);
      foreach ($array_map as $k => $value) {
        if (isset($_current[$value])) {
          $_current = (is_array($_current[$value]) || is_object($_current[$value]))
            ? (array) $_current[$value] : $_current[$value];
        }
        else {
          return;
        }
      }
      return $_current;
    }
    elseif (isset($_current[$argument])) {
      return $_current[$argument];
    }
  }
}
