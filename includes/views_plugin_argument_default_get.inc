<?php

/**
 * @file
 * Contains the get argument default plugin.
 */

/**
 * Default argument plugin to use the raw value from the URL.
 *
 * @ingroup views_argument_default_plugins
 */
class ViewsPluginArgumentDefaultGet extends ViewsPluginArgumentDefaultGlobals {

  /**
   * Provide the default form form for validating options.
   *
   * @param array $form
   *   Array of drupal form api.
   * @param string $form_state
   *   Values in form.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    // Using range(1, 10) will create an array keyed 0-9, which allows arg() to
    // properly function since it is also zero-based.
    $form['argument'] = array(
      '#type' => 'textfield',
      '#title' => t('Get Key'),
      '#default_value' => $this->options['argument'],
      '#description' => t('Example:') . '<br/>' .
      t('$_GET[variable] = variable') . '<br/>' .
      t('$_GET[arr][var] = arr:var') . '<br/>' .
      t('$_GET[arr][obj]->var = arr:obj:var'),
    );
    
  }

}
